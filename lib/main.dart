import 'package:flutter/material.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:carousel_slider/carousel_slider.dart';

void main() => runApp(MyApp());


class MyApp extends StatelessWidget {
  final routes = <String, WidgetBuilder>{

    MyHomePage.tag: (context) => MyHomePage(),
  };
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: AnimatedSplashScreen (),
    );
  }
}

class AnimatedSplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => new SplashScreenState();
}

class SplashScreenState extends State<AnimatedSplashScreen>
    with SingleTickerProviderStateMixin {
  var _visible = true;

  AnimationController animationController;
  Animation<double> animation;

  startTime() async {
    var _duration = new Duration(seconds: 5);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
    Navigator.push(context, new MaterialPageRoute(
        builder: (context) =>
        new LoginPage())
    );
  }

  @override
  void initState() {
    super.initState();
    animationController = new AnimationController(
        vsync: this, duration: new Duration(seconds: 4));
    animation =
    new CurvedAnimation(parent: animationController, curve: Curves.easeOut);

    animation.addListener(() => this.setState(() {}));
    animationController.forward();

    setState(() {
      _visible = !_visible;
    });
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Column(
            mainAxisAlignment: MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[

              Padding(padding: EdgeInsets.only(bottom: 30.0),child:new Image.asset('assets/images/logo.png',height: 25.0,fit: BoxFit.scaleDown,))


            ],),
          new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Image.asset(
                'assets/images/logo.png',
                width: animation.value * 250,
                height: animation.value * 250,
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',

      child: Image.asset('assets/images/logo.png',fit: BoxFit.contain, height: 50.0,),
    );

    final email = TextFormField(
      keyboardType: TextInputType.phone,
      autofocus: false,
      validator: (value){
        if (value.isEmpty){
          return 'please enter mobile number';
        }
      },

      decoration: InputDecoration(
        hintText: 'Mobile Number',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        //border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final password = TextFormField(
      autofocus: false,
      initialValue: 'some password',
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Password',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
//        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {

          Navigator.push(context, new MaterialPageRoute(
              builder: (context) =>
              new VerifyPage())
          );
        },
        padding: EdgeInsets.all(12),
        color: Colors.black,
        child: Text('Send OTP', style: TextStyle(color: Colors.white)),
      ),
    );

    final forgotLabel = FlatButton(
      child: Text(
        'Forgot password?',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () {},
    );

    return Scaffold(
      backgroundColor: Colors.grey[900],
      body: Center(
        child:Card(
          elevation: 5.0,
          margin: EdgeInsets.all(24.0),
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.all(24.0),
            children: <Widget>[
              logo,
              SizedBox(height: 54.0),
              email,
              SizedBox(height: 8.0),
              loginButton,

            ],
          ),
        ),

      ),
    );
  }
}
class VerifyPage extends StatefulWidget {
  static String tag = 'verify-page';
  @override
  _VerifyPageState createState() => new _VerifyPageState();
}

var isLoading = false;

class _VerifyPageState extends State<VerifyPage> {
  @override
  Widget build(BuildContext context) {
    final logo = Hero(
        tag: 'hero',

        child: Image.asset('assets/images/logo.png',height: 50.0,)

    );

    final email = TextFormField(
      keyboardType: TextInputType.number,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Verify OTP Number',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        // border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final password = TextFormField(
      autofocus: false,
      initialValue: 'some password',
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Password',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {

          Navigator.push(context, new MaterialPageRoute(
              builder: (context) =>
              new MyHomePage())
          );
        },
        padding: EdgeInsets.all(12),
        color: Colors.black,
        child: Text('Verify OTP', style: TextStyle(color: Colors.white)),
      ),
    );

    final forgotLabel = FlatButton(
      child: Text(
        'Forgot password?',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () {},
    );

    return Scaffold(
      backgroundColor: Colors.grey[900],
      body: Center(
        child:Card(
          elevation: 5.0,
          margin: EdgeInsets.all(24.0),
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.all(24.0),
            children: <Widget>[
              logo,
              SizedBox(height: 60.0),
              email,
              SizedBox(height: 8.0),
              loginButton,

            ],
          ),
        ),

      ),
    );
  }
}
class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  static String tag = 'my-home-page';

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}
int photoindex = 0;

List<String> photos = [
  'assets/images/1.jpg',
  'assets/images/2.jpg',


];



class _MyHomePageState extends State<MyHomePage> {
  List photosM =List();
  List photosN =List();
  List Slider=List();

  var data;
  var dataf,datas;
  Future getData() async {
    http.Response response = await http.get("https://sadathostings.online/videoapp/public/api/video-all");
    http.Response responsef= await http.get("https://sadathostings.online/videoapp/public/api/f-video-all");
    http.Response responses= await http.get("https://sadathostings.online/videoapp/public/api/slide-all");
    if(responses.statusCode==200){
      data = json.decode(response.body);
      dataf = json.decode(responsef.body);
      datas = json.decode(responses.body);
      setState(() {
        photosM = data;
        photosN = dataf;
        Slider=datas;
      });
    }
    else{
      print('Something went wrong. \nResponse Code : ${response.statusCode}');
    }

  }

  @override
  void initState() {
    super.initState();
    getData();
  }
  Widget build(BuildContext context) {
 Slider.add('https://sadathostings.online/videoapp/storage/app/slider/1552297542.maxresdefault.jpg');
    return DefaultTabController(
        length: 1,
        child: Scaffold(
          drawer: Drawer(
              child: new ListView(
                children: <Widget>[
                  Container(
                    height: 90.0,
                    child: DrawerHeader(child: new Image.asset(
                      'assets/images/logo.png',height:30.0,

                    ),
                    ),

                    padding: EdgeInsets.only(top: 8.0,left:0.0),
                    decoration: new BoxDecoration(
                        color: Colors.grey[900]
                    ),
                  ),
                  ExpansionTile(
                    leading: new Icon(Icons.category,color: Colors.orange,),
                    title: Text('Categories',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 15.0),
                    ),
                    children: <Widget>[
                      ListTile(

                        title:Text("Web Series",

                          style: TextStyle(
                              color: Colors.orange,
                              fontSize: 13.0),
                        ),


                      ),
                      ListTile(
                        title:Text("Short Films",

                          style: TextStyle(
                              color: Colors.orange,
                              fontSize: 13.0),
                        ),


                      ),
                      ListTile(
                        title: Text("Music Video",

                          style: TextStyle(
                              color: Colors.orange,
                              fontSize: 13.0),
                        ),
                      ),
                      ListTile(
                      title:Text("Movies",

                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 13.0),
                      ),
                      ),
                    ],
                  ),
                  ListTile(
                    leading: new Icon(Icons.share,color: Colors.blue),
                    title: Text('Share App',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 15.0),
                    ),
                  ),
                  ListTile(
                    leading: new Icon(Icons.feedback,color: Colors.redAccent),
                    title: Text('Feed Back',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 15.0),
                    ),
                  ),
                  ListTile(
                    leading: new Icon(Icons.arrow_back,color: Colors.green),
                    title: Text('Logout',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 15.0),
                    ),
                  ),

                ],


              )

          ),
          appBar: AppBar(
            backgroundColor: Colors.black,
            elevation: 0.0,
            actions: <Widget>[
              Row(
                children: <Widget>[
                  Text(
                    'Magento',
                    style: TextStyle(
                        color: Colors.orange,
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0),
                  ),
                  Text(
                    ' Movies',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0),
                  )
                ],
              ),
              SizedBox(
                width: 120.0,
              ),
//              IconButton(
//                  onPressed: () {
//                    debugPrint('Search button tapped');
//                  },
//                  icon: Icon(
//                    Icons.search,
//                    size: 35,
//                  ))
            ],
            bottom: TabBar(
              tabs: <Widget>[
                Tab(
                  text: 'Home',
                ),


              ],
            ),
          ),
          backgroundColor: Colors.black,
          body: ListView(
            shrinkWrap: true,

            children: <Widget>[
              Column(

                children: <Widget>[

                  Stack(

                    children: <Widget>[


                        CarouselSlider(
                          autoPlay: true,
                          autoPlayAnimationDuration: new Duration(seconds: 3),
                          viewportFraction: 1.0,
                          initialPage: 0,
                          height: 230.0,
                          pauseAutoPlayOnTouch: Duration(seconds: 3),
                          items: Slider.map((i) {
                            return Builder(
                              builder: (BuildContext context) {
                                return Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: EdgeInsets.symmetric(horizontal: 5.0),
                                    decoration: BoxDecoration(
                                        color: Colors.black,
                                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                                    ),
                                    child: new Image.network(i,fit: BoxFit.fill,)
                                );
                              },
                            );
                          }).toList(),
                        ),


                    ],
                  ),

                  SizedBox(height: 10.0),
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: Container(
                            height: 250.0,
                            color: Colors.grey[900],
                            child: Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          10.0, 10.0, 0.0, 10.0),
                                      child: Text(
                                        'Latest Videos',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 20.0
                                       ),
                                      ),
                                    )
                                  ],
                                ),
                                Column(
                                  children: <Widget>[
                                    Container(
                                      height: 200.0,
                                      child: ListView.builder(
                                        shrinkWrap: true,
                                        scrollDirection: Axis.horizontal,
                                        itemCount: photosM.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return Container(
                                            width: 140.0,
                                            child: Card(
                                              child: Image.network(photosM[index],
                                                  fit: BoxFit.fill),
                                            ),
                                          );
                                        },
                                      ),
                                    )
                                  ],
                                )
                              ],
                            )),
                      )

                    ],
                  ),
                  SizedBox(height: 10.0),
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: Container(
                            height: 250.0,
                            color: Colors.grey[900],
                            child: Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          10.0, 10.0, 0.0, 10.0),
                                      child: Text(
                                        'Featured Videos',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 20.0),
                                      ),
                                    ),

                                  ],
                                ),

                                Column(
                                  children: <Widget>[
                                    Container(
                                      height: 200.0,
                                      child: ListView.builder(
                                        shrinkWrap: true,
                                        scrollDirection: Axis.horizontal,
                                        itemCount: photosN.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return Container(
                                            width: 140.0,
                                            child: Card(
                                              child: Image.network(photosN[index],
                                                  fit: BoxFit.fill),
                                            ),
                                          );
                                        },
                                      ),
                                    )
                                  ],
                                )
                              ],
                            )),
                      )
                    ],
                  ),
                ],
              )
            ],
          ),
        ));
  }
}
